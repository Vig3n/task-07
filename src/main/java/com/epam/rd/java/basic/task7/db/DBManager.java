package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String URL = "jdbc:mysql://localhost:3306/test2db?user=root&password=root";
	private Connection con;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try (InputStream inputStream = new FileInputStream("app.properties"))
		{
			Properties properties = new Properties();
			properties.load(inputStream);
			con = DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			System.err.println("Cant connect to db");
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (ResultSet rs = con.createStatement().executeQuery("SELECT * FROM users"))
		{
			while (rs.next())
			{
				int id = rs.getInt("id");
				String login = rs.getString("login");
				//user.setId(rs.getInt("id"));
				//User user = new User(id, login);
				users.add(new User(id, login));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot find all", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null)
		{
			return false;
		}

		try (Statement st = con.createStatement();)
		{
			if (1 == st.executeUpdate("INSERT INTO users (login) VALUES ('" + user.getLogin() + "')", Statement.RETURN_GENERATED_KEYS))
			{
				try (ResultSet rs = st.getGeneratedKeys()){
					rs.next();
					user.setId(rs.getInt(1));
					return true;
				}
			}


		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot insert", e);
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users.length == 0)
		{
			return false;
		}
		PreparedStatement stmt = null;
		int deleted = 0;
		try {
			stmt = con.prepareStatement("DELETE FROM users WHERE ID = ?" , Statement.RETURN_GENERATED_KEYS);
			for(User u : users){
				stmt.setInt(1, u.getId());
				deleted = deleted + stmt.executeUpdate();
			}
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("Cannot delete users", e);
		}finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return deleted != 0;

	}

	public User getUser(String login) throws DBException {
		if (login == null)
		{
			return null;
		}
		try (ResultSet rs = con.createStatement().executeQuery("SELECT * FROM users WHERE login = '" + login + "'" )){
			if (rs.next())
			{
				User user = User.createUser(rs.getString("login"));
				if (user != null) {
					user.setId(rs.getInt("id"));
				}
				return user;
			}
			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot get user", e);
		}

	}

	public Team getTeam(String name) throws DBException {
		if (name == null)
		{
			return null;
		}
		try (ResultSet rs = con.createStatement().executeQuery("SELECT id, name FROM teams WHERE name = '" + name + "'" )){
			if (rs.next())
			{
				Team team = Team.createTeam(rs.getString("name"));
				if (team != null) {
					team.setId(rs.getInt("id"));
				}
				return team;
			}
			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot get team", e);
		}

	}

	public List<Team> findAllTeams() throws DBException {
		ArrayList<Team> teams = new ArrayList<>();

		try (ResultSet rs = con.createStatement().executeQuery("SELECT id, name FROM teams" )){
			while (rs.next())
			{
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("cannot find", e);
		}
		return teams;

	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null)
		{
			return false;
		}
		try (Statement st = con.createStatement()){
			if (1 == st.executeUpdate("INSERT INTO teams (name) VALUES ('" + team.getName() +  "')", Statement.RETURN_GENERATED_KEYS)){
				try(ResultSet rs = st.getGeneratedKeys()) {
					rs.next();
					team.setId(rs.getInt(1));
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot get team", e);
		}
		return false;

	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams == null)
		{
			return false;
		}
		try (PreparedStatement st = con.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (" + user.getId() + ",?)");
		)
		{
			con.setAutoCommit(false);
			boolean isInserted = false;

			for (Team t : teams)
			{
				if (t!= null)
				{
					st.setInt(1, t.getId());
					isInserted = (st.executeUpdate() > 0) || isInserted;
				}
			}
			con.commit();
			con.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			tryRollBack(con);
			throw new DBException("Cannot set team", e);
			//return false;
		}
//		finally {
//			tryClose(co);
//		}


	}

	private void tryRollBack(Connection con)
	{
		if (con != null)
		{
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}



	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();

		try (ResultSet rs = con.createStatement().executeQuery("SELECT team_id, name FROM users_teams JOIN teams ON team_id = id WHERE user_id = " + user.getId())){

			while (rs.next())
			{
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("team_id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot get team", e);
		}
		return teams;

	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null)
		{
			return false;
		}
		try (Statement st = con.createStatement()){
			return st.executeUpdate("DELETE FROM teams WHERE name = '" + team.getName() + "'") > 0;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot delete team", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		if (team == null)
		{
			return false;
		}
		try (Statement st = con.createStatement()){
			return st.executeUpdate("UPDATE teams SET name = '" + team.getName() + "' WHERE id = " + team.getId()) > 0;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot update team", e);
		}
	}

}
